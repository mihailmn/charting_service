from django.apps import AppConfig


class RevenueServiceConfig(AppConfig):
    name = 'charting_service'
