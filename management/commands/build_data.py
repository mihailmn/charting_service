from django.core.management.base import BaseCommand
from charting_service import models
import django.db
import xlrd


def build_branch():
    """build:
            - read data from file,
            - read one string with data branch from file,
            - create a record in table <Branch>
    """
    filename = 'charting_service/test_LT.xlsx'
    try:
        read_book = xlrd.open_workbook(filename, on_demand=True)
    except FileNotFoundError:
        print("File test_LT.xlsl not found")
        return
    read_sheet = read_book.sheet_by_index(3)
    for rownum in range(read_sheet.nrows):
        row = read_sheet.row_values(rownum)
        try:
            branch = models.Branch(name=row[0])
            branch.save()
        except django.db.IntegrityError:
            print('Try add existing values = ' + str(branch))
        except IndexError:
            print('Error index = ' + str(branch))
        except TypeError:
            print('Error type = ' + str(branch))
    print('build Ok')


def build_series():
    """build:
        - read data from file,
        - read one string with data series from file,
        - create a record in table <ModelSeries>"""
    filename = 'charting_service/test_LT.xlsx'
    try:
        read_book = xlrd.open_workbook(filename, on_demand=True)
    except FileNotFoundError:
        print("File test_LT.xlsl not found")
        return
    read_sheet = read_book.sheet_by_index(2)
    for rownum in range(1, read_sheet.nrows):
        row = read_sheet.row_values(rownum)
        try:
            c_series = models.ModelSeries(name=row[0])
            c_series.save()
        except django.db.IntegrityError:
            print('Try add existing values = ' + str(c_series))
        except IndexError:
            print('Error index = ' + str(c_series))
        except TypeError:
            print('Error type = ' + str(c_series))
        try:
            rate = models.RatePerKm(series=models.ModelSeries.objects.filter(name=c_series.name)[0], rate=row[1])
            rate.save()
        except django.db.IntegrityError:
            print('Try add existing values = ' + str(rate))
        except IndexError:
            print('Error index = ' + str(rate))
        except TypeError:
            print('Error type = ' + str(rate))
    try:
        c_series = models.ModelSeries(name='Все серии')
        c_series.save()
    except django.db.IntegrityError:
        print('Try add existing values = ' + str(c_series))
    except IndexError:
        print('Error index = ' + str(c_series))
    except TypeError:
        print('Error type = ' + str(c_series))
    print('build Ok')


def build_year():
    """ build:
            - read data from file,
            - create a record in table <Year>
    """
    filename = 'charting_service/test_LT.xlsx'
    try:
        read_book = xlrd.open_workbook(filename, on_demand=True)
    except FileNotFoundError:
        print("File test_LT.xlsl not found")
        return
    read_sheet = read_book.sheet_by_index(1)
    row = read_sheet.row_values(0)
    for c in row[2:]:
        try:
            year = models.Year(value=int(c))
            year.save()
        except django.db.IntegrityError:
            print('Try add existing values = ' + str(year))
        except IndexError:
            print('Error index = ' + str(year))
        except TypeError:
            print('Error type = ' + str(year))
    print('build Ok')


def build_kilometrage():
    """ build :
            - read data from file,
            - create a list of one string with data kilometrage
            - create a record in a table <Kilometrage>
    """
    filename = 'charting_service/test_LT.xlsx'
    l = []
    try:
        read_book = xlrd.open_workbook(filename, on_demand=True)
    except FileNotFoundError:
        print("File test_LT.xlsl not found")
        return
    read_sheet = read_book.sheet_by_index(1)

    for rownum in range(1, read_sheet.nrows):
        row = read_sheet.row_values(rownum)
        l.clear()
        for c in row:
            l.append(c)
        current_branch = models.Branch.objects.filter(name=l[0])
        current_series = models.ModelSeries.objects.filter(name=l[1])
        count = 2
        for i in models.Year.objects.all():
            try:
                kilometrage = models.Kilometrage(branch=current_branch[0], series=current_series[0], year=i,
                                                 value=l[count])
                count += 1
                kilometrage.save()
            except django.db.IntegrityError:
                print('Try add existing values' + str(kilometrage))
            except IndexError:
                print('Error index')
                print(kilometrage)
            except TypeError:
                print('Error type = ' + str(kilometrage))
    print('build Ok')


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        # This is necessary to prevent dupes
        print('Build branch')
        build_branch()
        print('Build series')
        build_series()
        print('Build year')
        build_year()
        print('Build kilometrage')
        build_kilometrage()
