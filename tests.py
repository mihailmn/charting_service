import unittest
from . import models
from django.urls import reverse, resolve
from django.test import Client


class TestBase(unittest.TestCase):
    def setUp(self):
        for i in range(100):
            self.branch = models.Branch.objects.create(name='TestBranch' + str(i))
            self.series = models.ModelSeries.objects.create(name='TestSeries' + str(i))
            self.year = models.Year.objects.create(value=i)
            self.rate = models.RatePerKm.objects.create(series=self.series, rate=3.77)

            for j in models.Year.objects.all():
                self.kilometrage = models.Kilometrage.objects.create(branch=self.branch, series=self.series, year=j,
                                                                     value=3.77)

    def test1(self):
        """test fuction calculate income"""
        test_kilometrage = models.Kilometrage.objects.filter(branch=self.branch, series=self.series)
        print(models.calculate_income(test_kilometrage))
        print(models.check_period(40, 50))

class TestUrls(unittest.TestCase):

    def setUp(self):
        self.client = Client()

    def test_unit_view_url(self):
        response = self.client.get('/123')
        self.assertEqual(response.status_code, 200)
