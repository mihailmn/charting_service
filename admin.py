from . import models
from django.contrib import admin

admin.site.register(models.Branch)
admin.site.register(models.ModelSeries)
admin.site.register(models.RatePerKm)
admin.site.register(models.Kilometrage)
admin.site.register(models.Year)
