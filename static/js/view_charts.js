var csrftoken = $("[name=csrfmiddlewaretoken]").val();
$(document).ready(function() {
    $('#target').submit(function(){
        // убираем класс ошибок с инпутов

        $('input').each(function(){
            $(this).removeClass('error_input');
        });
        // прячем текст ошибок
        $('.error').hide();
        // получение данных из полей
        var branch = $('#id_branch').val();
        var series= $('#id_series').val();
        var data_first = $('#id_data_first').val();
        var data_last = $('#id_data_last').val();
        $.ajax({
            // метод отправки
            type: "POST",
            // путь до скрипта-обработчика
            url: '',
            // какие данные будут переданы
            data: {
                'branch': branch,
                'series': series,
                'data_first': data_first,
                'data_last': data_last
            },
             headers:
             {
                "X-CSRFToken": csrftoken
             },
            // тип передачи данных
            dataType: "json",
            // действие, при ответе с сервера
            success: function(data){

            if(data.success === 'ok')
            {
                /*
                * BAR CHART
                * ---------
                */
                var data_chart = [];
                for(var i =0;i< data.keys.length;i++)
                {
                    var array = [data.keys[i],data.values[i]]
                    data_chart.push(array)
                    console.log(data_chart);

                }
                var bar_data = {
                  data : data_chart,
                  color: '#3c8dbc'
                }
                $.plot('#bar-chart', [bar_data], {
                  grid  : {
                    borderWidth: 1,
                    borderColor: '#f3f3f3',
                    tickColor  : '#f3f3f3'
                  },
                  series: {
                    bars: {
                      show    : true,
                      barWidth: 0.5,
                      align   : 'center'
                    }
                  },
                  yaxis : {
                    show: true
                  },
                  xaxis : {
                    mode      : 'categories',
                    tickLength: 0,
                    show:true
                  }
                })
                /* END BAR CHART */
                alert("График сформирован.Для филиала: " + data.branch + " серии:" + data.series);
                }
                else
                {
                    alert(data.success);

                }
            }
        });
        return false;
    });
});

