================
charting service
================
This service is intended for plotting data from a file "test_LT.xlsx".

Quick start.
------------

1. Add "charting_service" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'charting_service',
    ]
2. To install dependencies from a file "requeriments.txt".
3. Include the polls URLconf in your project urls.py like this::

    url(r'^charting_service/',include('charting_service.urls')),

4. Run `python manage.py migrate` to create the charting_service models.


5. Run 'python manage.py build_data' to populate tables with data from a file.

6. Start development server.

7. Visit http://127.0.0.1:8000/charting_service/.
