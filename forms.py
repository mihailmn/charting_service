from django import forms
from . import models

class TestForm(forms.Form):
    """filter form"""
    branch = forms.ModelChoiceField(queryset=models.Branch.objects.all(), label='Филиал')
    series = forms.ModelChoiceField(queryset=models.ModelSeries.objects.all(), label='Серия')
    data_first = forms.ModelChoiceField(queryset=models.Year.objects.all(), label='Год от')
    data_last = forms.ModelChoiceField(queryset=models.Year.objects.all(), label='Год до')
