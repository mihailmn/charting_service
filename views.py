from django.shortcuts import render
from django.views.generic.base import View
from . import forms
from . import models
from django.http import HttpResponse
from django.http import JsonResponse


class ViewCharts(View):
    template_name = 'chart.html'
    form = forms.TestForm

    def post(self, request):
        if request.is_ajax():
            context = {}
            branch = models.Branch.objects.filter(id=request.POST.get('branch'))
            series = models.ModelSeries.objects.filter(id=request.POST.get('series'))
            date_first = models.Year.objects.filter(id=request.POST.get('data_first'))
            date_last = models.Year.objects.filter(id=request.POST.get('data_last'))
            if not branch.exists() or not series.exists() or not date_first.exists() or not date_last.exists():
                context['success'] = 'empty data 1'
                return JsonResponse(context)
            if models.check_period(date_first[0].value, date_last[0].value):
                context['success'] = 'error range year'
                return JsonResponse(context)
            if not series[0].name == 'Все серии':
                list_kilometrage = models.Kilometrage.objects.filter(branch=branch[0], series=series[0],
                                                                     year__value__range=(
                                                                         date_first[0].value, date_last[0].value))
            else:
                list_kilometrage = models.Kilometrage.objects.filter(branch=branch[0],
                                                                     year__value__range=(
                                                                         date_first[0].value, date_last[0].value))
            if not list_kilometrage.exists():
                context['success'] = 'empty data 2'
                return JsonResponse(context)
            data = {}
            data = models.calculate_income(list_kilometrage)
            if not data:
                context['success'] = 'empty data 3'
                return JsonResponse(context)
            context['keys'] = list(data.keys())
            context['values'] = list(data.values())
            context['branch'] = list_kilometrage[0].branch.name
            context['series'] = list_kilometrage[0].series.name
            context['success'] = 'ok'
            return JsonResponse(context)

    def get(self, request):
        context = {}
        context['form'] = self.form
        return render(request, self.template_name, context)
