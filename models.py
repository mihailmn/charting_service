from django.db import models

# Create your models here.
class Branch(models.Model):
    class Meta:
        verbose_name = 'Филиал'
        verbose_name_plural = 'Филиалы'

    name = models.CharField(max_length=20, verbose_name='Название филиала', unique=True)

    def __str__(self):
        return self.name


class ModelSeries(models.Model):
    class Meta:
        verbose_name = 'Серия'
        verbose_name_plural = 'Серии'

    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name


class RatePerKm(models.Model):
    class Meta:
        verbose_name = 'Ставка за км'
        verbose_name_plural = 'Ставка за км'

    series = models.ForeignKey('ModelSeries', on_delete=models.SET_NULL, verbose_name="Серия", null=True)
    rate = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return str(self.series) + '=' + str(self.rate)


class Year(models.Model):
    class Meta:
        verbose_name = 'Год'
        verbose_name_plural = 'Года'
        ordering = ('value',)

    value = models.PositiveIntegerField(verbose_name='Значение', unique=True)

    def __str__(self):
        return str(self.value)

    def __unicode__(self):
        return self.value


class Kilometrage(models.Model):
    class Meta:
        unique_together = ('branch', 'series', 'year')
        verbose_name = 'Пробег'
        verbose_name_plural = 'Пробег'
        ordering = ('branch__name', 'series__name', 'year__value',)

    branch = models.ForeignKey('Branch', on_delete=models.SET_NULL, verbose_name='Филиал', null=True)
    series = models.ForeignKey('ModelSeries', on_delete=models.SET_NULL, null=True, verbose_name='Серия')
    year = models.ForeignKey('Year', on_delete=models.SET_NULL, null=True, verbose_name='Год')
    value = models.PositiveIntegerField('Километраж')

    def __str__(self):
        return str(self.branch.name) + ' ' + str(self.series) + ' ' + str(self.year) + ' ' + str(self.value)


def check_period(first, last):
    """checking the correct range input"""
    if last - first <= 0:
        return True
    return False


def calculate_income(list_kilometrage):
    """calculate income:
            -getting rate,
            -check availability,
            -check for all series,
            -calculate
    """
    dic = {}
    for i in range(list_kilometrage.count()):
        rate = RatePerKm.objects.filter(series=list_kilometrage[i].series)
        if rate.exists():
            income = rate[0].rate * list_kilometrage[i].value
        else:
            income = 0.0
        if str(list_kilometrage[i].year.value) in dic:
            dic[str(list_kilometrage[i].year.value)] = dic[str(list_kilometrage[i].year.value)] + income
        else:
            dic[str(list_kilometrage[i].year.value)] = income
    return dic
