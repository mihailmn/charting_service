from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'$',views.ViewCharts.as_view(), name ='charting_service'),
]